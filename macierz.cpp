// Tworzenie macierzy dynamicznej
// Data: 8.02.2011
// (C)2012 mgr Jerzy Wa�aszek
//-----------------------------

#include <iostream>
#include <iomanip>

#include <iostream>
#include <iomanip>

// Procedura wype�nia macierz i wy�wietla j�
//------------------------------------------
void p(int m, int n, int ** A)
{
  int i,j;

  for(i = 0; i < m; i++)
    for(j = 0; j < n; j++) A[i][j] = (i + j) % 2;

  for(i = 0; i < m; i++)
  {
    for(j = 0; j < n; j++) cout << A[i][j] << " ";
    cout << endl;
  }
}

// *** PROGRAM G��WNY ***
//-----------------------

int main()
{
  int ** A,n,m,i;

  cout << "m = "; cin >> m;
  cout << "n = "; cin >> n;

  // tworzymy tablic� wska�nik�w

  A = new int * [m];

  // tworzymy kolejne tablice wierszy

  for(i = 0; i < m; i++) A[i] = new int [n];

  // wype�niamy i wy�wietlamy macierz

   p(m,n,A);

  // najpierw usuwamy tablice wierszy

  for(i = 0; i < m; i++) delete [] A[i];

  // teraz usuwamy tablic� wska�nik�w

  delete [] A;

  return 0;
}
