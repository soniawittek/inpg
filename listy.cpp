// Transpozycja grafu
// Data: 22.01.2014
// (C)2014 mgr Jerzy Wa�aszek
//---------------------------

#include <iostream>
#include <iomanip>



// Typy dla dynamicznej tablicy list s�siedztwa

struct slistEl
{
  slistEl * next;
  int v;
};

// **********************
// *** Program g��wny ***
// **********************

int main()
{
  int n,m;                   // Liczba wierzcho�k�w i kraw�dzi
  slistEl **A, **AT;         // Tablice list s�siedztwa
  int i,v,u;
  slistEl *p,*r;

  cin >> n >> m;             // Odczytujemy liczb� wierzcho�k�w i kraw�dzi

  A  = new slistEl * [n];    // Tworzymy tablice dynamiczne
  AT = new slistEl * [n];

  // Inicjujemy tablice

  for(i = 0; i < n; i++) A[i] = AT[i] = NULL;

  // Odczytujemy kolejne definicje kraw�dzi.

  for(i = 0; i < m; i++)
  {
    cin >> v >> u;           // Wierzcho�ki tworz�ce kraw�d�
    p = new slistEl;         // Tworzymy nowy element
    p->v = u;                // Numerujemy go jako w
    p->next = A[v];          // Dodajemy go na pocz�tek listy A[v]
    A[v] = p;
  }

  // Wyznaczamy graf transponowany

  for(v = 0; v < n; v++)     // Przegl�damy kolejne wierzcho�ki
    for(p = A[v]; p; p = p->next) // Przegl�damy s�siad�w v
    {
      r = new slistEl;       // Tworzymy nowy element listy
      r->v = v;              // Zapami�tujemy w nim wierzcho�ek v
      r->next = AT[p->v];    // i dodajemy do listy s�siada
      AT[p->v] = r;
    }

  // Wypisujemy wyniki

  cout << endl;
  for(v = 0; v < n; v++)
  {
    cout << v << " :";
    for(p = AT[v]; p; p = p->next) cout << " " << p->v;
    cout << endl;
  }

  // Usuwamy tablice dynamiczne

  for(i = 0; i < n; i++)
  {
    p = A[i];
    while(p)
    {
      r = p;
      p = p->next;
      delete r;
    }
    p = AT[i];
    while(p)
    {
      r = p;
      p = p->next;
      delete r;
    }
  }
  delete [] A;
  delete [] AT;

  return 0;
}
